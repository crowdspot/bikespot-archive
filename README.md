# BikeSpot Archive Map

This system displays the submissions made to the BikeSpot 2023 campaign for the
purposes of longer-term publishing.


# Data (from cleaned export)

Import from manually cleaned dataset:

    python3 process_cleaned_geojson.py cleaned.json


## Data (from Shareabouts snapshot)

*Note: The live map currently uses a cleaned dataset, not a direct Shareabouts snapshot.*

Patch Shareabouts API code as follows:

    diff --git a/src/sa_api_v2/serializers.py b/src/sa_api_v2/serializers.py
    index f632c96a..2bf76dca 100644
    --- a/src/sa_api_v2/serializers.py
    +++ b/src/sa_api_v2/serializers.py
    @@ -783,7 +783,7 @@ class BasePlaceSerializer (SubmittedThingSerializer, serializers.ModelSerializer

         def set_to_representation(self, set_name, submissions):
             serializer = SimpleSubmissionSerializer(submissions, many=True)
    -        serializer.initialize(parent=self, field_name=None)
    +        serializer.bind(parent=self, field_name='submission_set')
             return serializer.data

         def get_detailed_submission_sets(self, place):

Download a data snapshot from your Shareabouts instance, like this:

    https://[hostname]/api/v2/[user]/datasets/[dataset]/places/snapshots?include_submissions

This require a Celery worker to be running, like:

    celery --workdir=src --app=project worker -E --loglevel=INFO

Currently requires manual conversion as the snapshot is returned as Python code
containing JSON, eg. `b'...'` rather than JSON. Save as `raw_export.json` and
run:

    python3 process_snapshot_geojson.py raw_export.json
    
Output is automatically placed in the `public/spots.json` as expected by the app.


## Build for development

    npx shadow-cljs watch app
    npx tailwind --input=tailwind/input.css --output=public/output.css --watch

Visit: http://localhost:9090

Alternatively, for an editor-connected REPL in Emacs, run:

    cider-jack-in-cljs


## Build for production

    bin/build


## Format with cljfmt

See https://github.com/weavejester/cljfmt for globally installing cljfmt. Then run:

    clj -Tcljfmt check
    clj -Tcljfmt fix
