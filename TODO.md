# To-do

* use CLI upload to AWS Amplify from awscli v2
* include a hash in JS file name - see https://shadow-cljs.github.io/docs/UsersGuide.html#NameHashing
* bundle Mapbox CSS - build hook that copies relevant assets to public? `esbuild src/css.css --bundle --outfile=build/css.css --entry-names=[dir]/[name]-[hash] --metafile=meta.json --minify --analyze=verbose`
* split out vendor JS from app JS
* version that uses ornament
* map tiles not loading on mobile during dev
* contribute Shareabouts snapshot fixes upstream - `bind` and `b'...'`


# For next time

* can the map be made not to reload on every update? would it still update
  pop-up code?
* dev-only switches to automatically zoom to and click a spot


# Done

* add Tailwind build step (rather than development JS) see https://github.com/jacekschae/shadow-cljs-tailwindcss
* geocoder - like YourGround
* CrowdSpot branding
* Initial pop-up describing BikeSpot project
* Display BikeSpot 2023 data spots
* Bubble styling based on total comments + likes
* Colouring based on spot type
* Legend describing size/colour scheme
* Clickable pop-up showing various text content
* Deployed to AWS Amplify (similar to RiskyRoads 2022)
* Custom domain
