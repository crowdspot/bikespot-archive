import json
import sys

# For use with Anthony's manually cleaned GeoJSON.
if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} GEOJSONFILE', file=sys.stderr)
    exit(1)
with open(sys.argv[1]) as input_file:
    raw_data = json.load(input_file)

c = {
    'type': 'FeatureCollection',
    'features': [],
}

for raw_feature in raw_data['features']:
    props = raw_feature['properties']
    comments = [props[k] for k in props.keys() if k.startswith('Comment ')
                if props[k] is not None]
    f = {
        'type': 'Feature',
        'geometry': {
            'type': 'Point',
            'coordinates': [
                round(c, 5) for c in raw_feature['geometry']['coordinates']
            ],
        },
        'properties': {
            't': 1 if props['Location Type'] == 'safe spot' else 0,
            'l': props['Name'],
            'd': props['Description'],
            's': props.get('Submitter Name', None),
            'nc': props['Comments'],
            'ns': props['Supports'],
            'c': comments,
        }
    }
    c['features'].append(f)

with open('public/spots.json', 'w') as output_file:
    # Specifying separators gives more compact output.
    json.dump(c, output_file, separators=(',', ':'))

num_features = len(c['features'])
print(f'Processed {num_features} features.')
