import json
import sys

# As exported with:
# https://api.crowdspot.com.au/api/v2/admin/datasets/bikespot2023/places/snapshots?include_submissions&new
#
# Note: Required an additional step to convert the b'' format of the snapshot
# file into regular JSON file.
if len(sys.argv) != 2:
    print(f'Usage: {sys.argv[0]} GEOJSONFILE', file=sys.stderr)
    exit(1)
with open(sys.argv[1]) as input_file:
    raw_data = json.load(input_file)

c = {
    'type': 'FeatureCollection',
    'features': [],
}

for raw_feature in raw_data['features']:
    props = raw_feature['properties']
    if not props['visible']:
        continue
    f = {
        'type': 'Feature',
        'geometry': {
            'type': 'Point',
            'coordinates': [
                round(c, 5) for c in raw_feature['geometry']['coordinates']
            ],
        },
        'properties': {
            't': props['location_type'],
            'l': props['name'],
            'd': props['description'],
            'n': props['submitter_name']
        }
    }
    try:
        f['properties']['ns'] = len(props['submission_sets']['support'])
    except KeyError:
        f['properties']['ns'] = 0
    try:
        comments = props['submission_sets']['comments']
    except KeyError:
        comments = []
    f['properties']['nc'] = len(comments)
    f['properties']['c'] = [
        {
            't': c['comment'],
            'n': c['submitter_name']
        } for c in comments
    ]
    c['features'].append(f)

with open('public/spots.json', 'w') as output_file:
    # Specifying separators gives more compact output.
    json.dump(c, output_file, separators=(',', ':'))

num_features = len(c['features'])
print(f'Processed {num_features} features.')
