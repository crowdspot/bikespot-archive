/** @type {import('tailwindcss').Config} */
module.exports = {
  content: process.env.NODE_ENV == 'production' ? ["./public/js/main.js"] : ["./src/main/**/*.cljs"],
  theme: {
    extend: {
      colors: {
        // Keep aligned with archive_map/colours.cljs
        'safe': '#5cbb30',
        'unsafe': '#efac2b',
        'safe-dark': '#1d9834',
        'unsafe-dark': '#e4732c',
      },
    },
  },
  plugins: [],
}
