(ns archive-map.map
  "Map functionality"
  (:require [archive-map.colours :as colours]
            [clojure.string :as str]
            ["@mapbox/mapbox-gl-geocoder" :as MapboxGeocoder]
            ["map-gl-utils$default" :as U]
            ["mapbox-gl" :as mapboxgl]
            [reagent.dom.server :refer [render-to-static-markup]]))

;; Newer maplibre-gj doesn't support "mapbox:" URLs, raising a network error.
;; Using mapbox-gl because maplibre-gl fails on this style due to "pitch" value.
(def map-token "pk.eyJ1IjoiY3Jvd2RzcG90MSIsImEiOiIwTFItekZ3In0.M_0CqFroH04TUc-K1_CHXQ")
(def map-style (str "https://api.mapbox.com/styles/v1/crowdspot1/clw07sixq01xh01rj7d65127c?access_token=" map-token))

(defn pluralise [text n]
  (if (= n 1)
    text
    (str text "s")))

(defn popup-html
  "Render HTML for use in map popup.
  Using render-to-static-markup so we can stick with hiccup syntax."
  [{type :t
    location :l
    description :d
    submitter :s
    num-supports :ns
    num-comments :nc
    comments :c}]
  (render-to-static-markup
   [:div {:style
          {:font-family "\"Open Sans\", sans-serif !important"}}
    [:h4.text-sm.bold.text-center.py-3.px-4
     {:class (if (= type 1) "bg-safe" "bg-unsafe")}
     location]
    [:div.py-3.px-4 {:style {:max-height 400 :overflow-y "auto"}}
     [:div.text-base.mb-1
      [:span.text-5xl.text-slate-300.inline-block.pt-2.mr-1.mt-3
       {:style {:line-height "0.1rem"
                :vertical-align "-0.8rem"}}
       "“"]
      [:span.italic.pt-2 description]]
     [:div.text-sm.text-right "— "
      (if (str/blank? submitter) "Anonymous" submitter)]
     (when (or (> num-supports 0) (> num-comments 0))
       [:div.text-sm.center.mt-2
        (when (> num-supports 0) [:span num-supports (pluralise " support" num-supports)])
        (when (and (> num-supports 0) (> num-comments 0)) ", ")
        (when (> num-comments 0) [:span num-comments (pluralise " comment" num-comments)])])
     ;; mapbox-gl treats GeoJSON property values as strings (we need to
     ;; explicitly parse nested data).
     (for [[i comment] (map-indexed vector (js->clj (.parse js/JSON comments)))]
       [:div.text-sm.pt-4.mt-4
        {:key i
         :style {:border-top "1px solid #ddd"}}
        [:span.pt-2 comment]])]]))

(defn add-map-data [^js/Object map db]
  (.. map (addSource "spots" #js {:type "geojson"
                                  :data "/spots.json"
                                  :generateId true}))
  (.. map (addLayer
           (clj->js {:id "spots-circles"
                     :type "circle"
                     :source "spots"
                     :paint {:circle-color ["match"
                                            ["get" "t"]
                                            [0]
                                            colours/unsafe
                                            colours/safe]
                             :circle-stroke-color ["match"
                                                   ["get" "t"]
                                                   [0]
                                                   colours/unsafe-dark
                                                   colours/safe-dark]
                             :circle-stroke-width 0.75
                             :circle-opacity 0.5
                             :circle-radius ["interpolate"
                                             ["linear"]
                                             ["zoom"]
                                             0 3
                                             5.01 3
                                             10 3
                                             11.49 ["interpolate"
                                                    ["linear"]
                                                    ["+"
                                                     ["to-number" ["get" "ns"]]
                                                     ["to-number" ["get" "nc"]]]
                                                    0 8
                                                    10 9
                                                    20 10
                                                    30 15
                                                    40 20
                                                    60 25]
                                             22 ["interpolate"
                                                 ["linear"]
                                                 ["+"
                                                  ["to-number" ["get" "ns"]]
                                                  ["to-number" ["get" "nc"]]]
                                                 0 10
                                                 10 20
                                                 20 30
                                                 30 40
                                                 40 50
                                                 60 70
                                                 78 90]]}})))
  (.. map (addSource "selected" #js {:type "geojson" :data #js {:type "FeatureCollection" :features #js []}}))
  (.. map (addLayer
           (clj->js {:id "selected-circles"
                     :type "circle"
                     :source "selected"
                     :paint {:circle-stroke-color "white"
                             :circle-stroke-width 4
                             ;; Like the spot style above, but with 10 units added to radius
                             :circle-radius ["interpolate"
                                             ["linear"]
                                             ["zoom"]
                                             0 13
                                             5.01 13
                                             10 13
                                             11.49 ["interpolate"
                                                    ["linear"]
                                                    ["+"
                                                     ["to-number" ["get" "ns"]]
                                                     ["to-number" ["get" "nc"]]]
                                                    0 13
                                                    10 16
                                                    20 19
                                                    30 22
                                                    40 25
                                                    60 30]
                                             22 ["interpolate"
                                                 ["linear"]
                                                 ["+"
                                                  ["to-number" ["get" "ns"]]
                                                  ["to-number" ["get" "nc"]]]
                                                 0 13
                                                 10 16
                                                 20 20
                                                 30 25
                                                 40 30
                                                 60 40
                                                 78 50]]
                             :circle-color "transparent"}})))
  (.. map (on "click" "spots-circles"
              (fn [e]
                (let [first-feature (aget (.-features e) 0)]
                  (swap! db assoc :spot first-feature)
                  (.. (mapboxgl/Popup. #js {:closeButton false :maxWidth "350px"})
                      (setHTML (popup-html (js->clj (.-properties first-feature) {:keywordize-keys true})))
                      (setLngLat e.lngLat)
                      (addTo map)
                      ;; Blank out the selected dataset
                      (on "close" #(.. map -U (setData "selected" #js {:type "FeatureCollection" :features #js []}))))
                  ;; Add selected feature to selected dataset to draw white outline circle
                  (.. map -U (setData "selected" #js {:type "Feature"
                                                      :properties (.. first-feature -properties)
                                                      :geometry (.. first-feature -geometry)}))))))
  ;; Similar to map-gl-utils hoverPointer.
  (.. map (on "mouseenter" "spots-circles" #(set! (.. map getCanvas -style -cursor) "pointer")))
  (.. map (on "mouseleave" "spots-circles" #(set! (.. map getCanvas -style -cursor) "")))
  ;; Good use of map-gl-utils - hover effects are harder than they look.
  (.. map -U (hoverFeatureState "spots-circles")))

(defn shrink-search []
  (let [search-control (js/document.querySelector ".mapboxgl-ctrl-geocoder")
        search-input (js/document.querySelector ".mapboxgl-ctrl-geocoder--input")]
    (set! search-control.style.minWidth 0)
    (set! search-control.style.cursor "pointer")
    (set! search-input.style.width "36px")
    (set! search-input.style.paddingLeft 0)
    (set! search-input.style.paddingRight 0)
    (set! search-input.style.visibility "hidden")))

(defn expand-search []
  (let [search-control (js/document.querySelector ".mapboxgl-ctrl-geocoder")
        search-input (js/document.querySelector ".mapboxgl-ctrl-geocoder--input")]
    (set! search-control.style.minWidth "240px")
    (set! search-control.style.cursor "")
    (set! search-input.style.width "")
    (set! search-input.style.paddingLeft "45px")
    (set! search-input.style.paddingRight "45px")
    (set! search-input.style.visibility "")
    (.focus search-input)))

(defn add-expanding-search []
  (shrink-search)
  (let [search-control-elem (js/document.querySelector ".mapboxgl-ctrl-geocoder")
        search-input-elem (js/document.querySelector ".mapboxgl-ctrl-geocoder--input")]
    (.addEventListener search-control-elem "click" #(expand-search))
    (.addEventListener search-input-elem "blur" #(shrink-search))))

(defn initialise-map [map-container-ref ^js/object map-ref db {:keys [lat lng]} setPosition zoom setZoom]
  (js/console.log "Initialising map")
  (set! mapboxgl/accessToken map-token)
  (set! (.-current map-ref)
        (mapboxgl/Map.
         (clj->js {:container (.-current map-container-ref)
                   :style map-style
                   :center [lng lat]
                   :zoom zoom
                   :minZoom 2
                   :maxZoom 19
                   :maxBounds [[100 -50.5] [168 -1]]  ; [SW NE]
                   :pitchWithRotate false
                   :projection "mercator"  ; Avoid the default 3D projection in mapbox-gl.
                   :dragRotate false
                   :doubleClickZoom false
                   :boxZoom false
                   :customAttribution "<a href=\"https://gitlab.com/crowdspot/bikespot-archive\">Source</a>"})))
  (.init U (.-current map-ref))  ; For use in add-map-data
  (.. map-ref -current (addControl (MapboxGeocoder. #js {:accessToken map-token
                                                         :mapboxgl mapboxgl
                                                         :placeholder "Search address"})
                                   "top-right"))
  ;; Add expanding effect to geocoder search box on mobile.
  (when (< js/window.innerWidth 640)
    (add-expanding-search))
  (.. map-ref -current (addControl (mapboxgl/NavigationControl.) "top-left"))
  (.. map-ref -current (on "load" #(add-map-data (.. map-ref -current) db)))
  ;; Handlers to update the map info in the debug panel.
  (.. map-ref -current (on "zoom" #(setZoom (.. map-ref -current getZoom (toFixed 2)))))
  (.. map-ref -current (on "move" #(setPosition
                                    {:lat (.. map-ref -current getCenter -lat (toFixed 2))
                                     :lng (.. map-ref -current getCenter -lng (toFixed 2))}))))

(defn cleanup-map [^js/object map-ref]
  (js/console.log "Cleaning up map")
  (.. map-ref -current remove))
