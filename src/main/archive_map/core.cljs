;; BikeSpot Archive Map
;; Copyright 2024 Ben Sturmfels

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.
(ns archive-map.core
  "BikeSpot 2023 archive map."
  (:require [archive-map.views :as views]
            [archive-map.map :as map]
            [goog.events :as events]
            ["react" :as react]
            [reagent.dom :as rdom]
            [reagent.core :as reagent])
  (:import [goog.events EventType]))

(defn show-intro []
  (.. (js/document.querySelector "#intro") showModal))

(defn show-signup []
  (.. (js/document.querySelector "#signup") showModal))

(defn app
  "The main application Reagent/React component.

  Based on
  https://clojurians-log.clojureverse.org/shadow-cljs/2023-01-15
  https://github.com/thheller/reagent-pdfjs/blob/master/src/main/demo/app.cljs#L19-L22
  https://docs.mapbox.com/help/tutorials/use-mapbox-gl-js-with-react/
  https://react.dev/learn/synchronizing-with-effects#controlling-non-react-widgets"
  [db]
  (let [;; React hooks must only be called at the top-level of React
        ;; components. This component must also be used with :f>.
        ;;
        ;; You give the :ref attribute a callback or hook and it will receive a
        ;; reference to the current component when it's mounted.
        ;;
        ;; Refs allow you to store data on the component which is maintained
        ;; between renders. Unlike state, their contents is regular mutable JS
        ;; data and changing them doesn't trigger a re-render. An alternative to
        ;; useRef would be to use a standard Clojure atom (not Reagent atom) in
        ;; a form-2 component and store the map references there. Or a global
        ;; variable would probably work too.
        map-container-ref (react/useRef nil)  ; Returns #js {:current nil}
        map-ref (react/useRef nil)
        ;; State persists between renders and changing it with the setter
        ;; function will trigger a re-render.
        ;; See: https://react.dev/learn/state-a-components-memory#how-does-react-know-which-state-to-return
        [position setPosition] (react/useState {:lat -25.734968 :lng 134.489563}) ; centre of Australia
        [zoom setZoom] (react/useState 3)]
    ;; Effects are used to syncronise with an external system by running some
    ;; impure code after rendering.
    ;;
    ;; Runs after *every* render by default, but you can specify an array of
    ;; dependencies so that use useEffect will only re-run if these
    ;; dependencies change. Since this is specified but empty, useEffect won't
    ;; re-run.
    (react/useEffect
     ;; Closure so we can the above locals are in scope.
     (fn []
       (map/initialise-map map-container-ref map-ref db position setPosition zoom setZoom)
       ;; Return a useEffect cleanup function - run automatically when unmounting.
       #(map/cleanup-map map-ref))
     ;; No dependencies, so useEffect is not re-run.
     #js [])
    [:div.flex.flex-col.text-slate-900
     ;; Accommodate the toolbars on iOS Safari
     {:style {:height "100dvh"}}
     [:f> views/intro]
     [views/signup]
     [views/header show-intro show-signup]
     [:div.grow.relative
      [:div.absolute
       {:style {:width 300
                ;; See notes about re-rendering issues below in `start`.
                :display (if (:debug-panel @db) "" "none")
                :z-index 100}}
       [views/sidebar db (.-current map-ref) position zoom]]
      ;; Use of the React will update map-container.current with the DOM node created. That
      ;; way we have somewhere to mount the map, and the useEffect handler only
      ;; runs *after* this happens, avoiding any timing issues where the element
      ;; doesn't yet exist.
      [:div.w-full.h-full {:ref map-container-ref
                           :style {:background "#5D757E"}}]
      [:div.absolute {:style {:bottom 40 :left 8}}
       [views/legend]]]
     [views/footer]]))

(defonce db (reagent/atom
             {:spot nil
              :debug-panel false}))

(defn toggle-debug-panel [e]
  (when (= (.-key e) "`")
    (.preventDefault e)
    (swap! db update :debug-panel not)))

(defn ^:dev/after-load start []
  (js/console.info "archive-map.core/start")
  (rdom/render
   ;; :> is a shortcut for for `adapt-react-class`, which automatically converts
   ;; the ClojureScript parameters to JS objects.
   ;; :f> is a shortcut to create React function components from
   ;; Reagent components (React hooks only work with function
   ;; components).
   (if goog.DEBUG
     ;; It takes three presses of the "`" key to cause the debug panel to show
     ;; when StrictMode is enabled. This appears to be the combination of
     ;; StrictMode, a React functional component and possibly the Reagent
     ;; atom. The app function is called and sees the changed db, but still
     ;; doesn't re-render. If I remove StrictMode or turn it into a Reagent
     ;; component, things work fine. It's fine after the initial three changes,
     ;; but same happens after hot-reload. Possibly it's not ideal to be
     ;; partially using hooks and partially using ratoms.
     [:> react/StrictMode [:f> app db]]
     [:f> app db])
   (js/document.querySelector "#root")))

(defn init []
  (js/console.info "archive-map.core/init")
  (events/listen js/document EventType.KEYPRESS toggle-debug-panel)
  (start)
  (js/window.setTimeout show-intro 100))
