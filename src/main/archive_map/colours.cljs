(ns archive-map.colours)

;; Keep aligned with tailwind.config.js
(def safe "#5cbb30")
(def unsafe "#efac2b")
(def safe-dark "#1d9834")
(def unsafe-dark "#e4732c")
