(ns archive-map.views
  (:require [kitchen-async.promise :as p]
            [lambdaisland.fetch :as fetch]
            [reagent.core :as r]
            ["react" :as react]))

(defn intro []
  (let [dialog-ref (react/useRef nil)]
    (fn []
      [:dialog#intro
       {:ref dialog-ref
        :on-click (fn [e]
                    ;; Close the dialog if you click on it directly (the
                    ;; backdrop). There's a <div> covering the dialog itself.
                    ;; "this" isn't available in React so we need to use a ref.
                    ;; See https://stackoverflow.com/a/72916231
                    (let [this (.-current dialog-ref)
                          target (.-target e)]
                      (when (= this target)
                        (.close this))))}
       [:div.p-4.md:p-6.mx-2.md:mx-auto.mt-2.md:mt-auto.rounded-md
        {:style {:max-width 600}}
        [:h4.text-xl.font-bold.leading-none.mb-5 "BikeSpot Archive Map"]
        [:p.mb-3 "BikeSpot is a digital mapping campaign that allows every Australian to say where they feel safe or unsafe while riding their bike. BikeSpot 2023 launched on 19 Oct 2023 and submissions closed on 31 Jan 2024. This follows previous smaller BikeSpot projects in 2016 and 2020."]
        [:p.mb-3 "This map represents a visual archive of the BikeSpot 2023 dataset (de-identified), which contains 72,844 submissions encompassing safe/unsafe spots, comments, and supports.  "]
        [:p.mb-3 "We hope planners and advocates use this visualisation, dataset, and other insights to inform priority locations and designs for future safe cycling infrastructure."]
        [:p.mb-3 "Please visit " [:a.underline {:href "https://www.bikespot.org" :target "_blank"} "bikespot.org"] " for more information and to sign-up for the latest news."]
        [:form.mt-6 {:method "dialog"}
         [:button.text-xl.text-white.bg-safe.hover:bg-safe-dark.transition-all.font-bold.w-full.p-3.rounded-sm
          "Explore the map"]]
        [:p.italic.mt-6 "We acknowledge the traditional owners and custodians of
 country throughout Australia and acknowledge their continuing connection to
 land, waters and community. We pay our respects to the people, the cultures and
 the elders past, present and emerging."]]])))

(defn signup-pre-release
  "Temporary version of the popup for use prior to the data release."
  []
  [:dialog#signup.p-4.md:p-6.mx-2.md:mx-auto.mt-2.md:mt-auto.rounded-md
   {:style {:max-width 400}}
   [:h4.text-xl.font-bold.leading-none.mb-5 "BikeSpot 2023 national dataset"]
   [:p.text-base.mb-5 "The BikeSpot 2023 de-identified data will be made available soon. To stay up to date on the release, please sign-up to the newsletter via " [:a.underline {:href "https://www.bikespot.org" :target "_blank"} "bikespot.org"] "."]
   [:button.text-l.text-white.bg-safe.hover:bg-safe-dark.transition-all.font-bold.p-3.rounde-sm
    {:on-click (fn [_] (.close (js/document.querySelector "#signup")))}
    "Explore the map"]])

(defn signup []
  (let [state (r/atom {:name ""
                       :email ""
                       :organisation ""
                       :status :idle})
        ;; Similar to intro, but using atom instead of useRef.
        dialog-ref (clojure.core/atom nil)]
    (fn []
      (let [{:keys [name email organisation status]} @state]
        [:dialog#signup
         {:ref (fn [elem] (reset! dialog-ref elem))
          :on-click (fn [e]
                      (let [this @dialog-ref
                            target (.-target e)]
                        (when (= this target)
                          (.close this))))}
         [:div.p-4.md:p-6.mx-2.md:mx-auto.mt-2.md:mt-auto.rounded-md
          {:style {:max-width 400}}
          [:h4.text-xl.font-bold.leading-none.mb-5 "Request BikeSpot 2023 national dataset"]
          [:p.text-base.mb-5 "Please enter your details below to receive links to the de-identified data via email."]
          [:form
           {:on-submit (fn [e]
                         (.preventDefault e)
                         (swap! state assoc :status :sending)
                         (p/try
                           (p/let [res (fetch/post
                                        "https://api.crowdspot.com.au/business/signup/bikespot-data-download/"
                                        {:content-type :json
                                         :accept :json
                                         :body {:name name
                                                :email email
                                                :organisation organisation}})]
                             (if (= (:status res) 201)
                               (do
                                 (swap! state assoc :status :sent)
                                 (js/setTimeout (fn []
                                                  (.close (js/document.querySelector "#signup"))
                                                  (reset! state {:name "" :email "" :organisation "" :status :idle}))
                                                2000))
                               (do
                                 (swap! state assoc :status :error)
                                 (js/setTimeout #(swap! state assoc :status :idle) 5000))))
                           (p/catch :default exc
                             (js/console.log "Error!" exc)
                             (do
                               (swap! state assoc :status :error)
                               (js/setTimeout #(swap! state assoc :status :idle) 5000)))))}
           [:div.flex.flex-col.gap-2.mb-5
            [:input.w-full.md:w-auto.px-2.py-1.border-2.border-slate-400.rounded
             {:value name
              :auto-focus true
              :type "text"
              :required true
              :placeholder "Name"
              :on-change (fn [e]
                           (swap! state assoc :name (.. e -target -value)))}]
            [:input.w-full.md:w-auto.px-2.py-1.border-2.border-slate-400.rounded
             {:value email
              :type "email"
              :required true
              :placeholder "Email"
              :on-change (fn [e]
                           (swap! state assoc :email (.. e -target -value)))}]
            [:input.w-full.md:w-auto.px-2.py-1.border-2.border-slate-400.rounded
             {:value organisation
              :type "text"
              :required false
              :placeholder "Organisation"
              :on-change (fn [e]
                           (swap! state assoc :organisation (.. e -target -value)))}]]
           [:button.text-l.text-white.transition-all.font-bold.p-3.rounded-sm
            {:class (if (not= status :sending)
                      "bg-safe hover:bg-safe-dark"
                      "bg-slate-200 hover:bg-slate-300")
             :disabled (= status :sending)}
            "Request data"]
           [:button.text-l.text-white.transition-all.font-bold.bg-slate-400.hover:bg-slate-500.p-3.ml-4.rounded-sm
            {:disabled (= status :sending)
             :on-click (fn [e]
                         (.preventDefault e)
                         (.close (js/document.querySelector "#signup")))}
            "Cancel"]
           (when (= status :sent) [:div.text-safe-dark.mt-3 "Requested!"])
           (when (= status :error) [:div.mt-3 {:style {:color "red"}} "Sorry couldn't send!"])]]]))))

(defn sidebar [db ^js/object map position zoom]
  [:div#sidebar.bg-slate-100.px-4.py-2
   (let [{:keys [spot]} @db]
     [:div
      [:button.block.bg-slate-300.p-2.w-full.mb-2
       {:on-click (fn [] (.. map (panTo #js [134.489563 -25.734968] #js {:zoom 4})))}
       "Centre"]
      [:div "Selected spot: " (if spot (.-id spot) " –")]
      [:div (str "Lat/Lng: " (:lat position) " / " (:lng position))]
      [:div (str "Zoom: " zoom)]])])

(defn legend []
  [:div.text-white.text-sm.p-3.rounded-sm
   {:class "bg-[#404854]/75"
    :style {:box-shadow "1px 1px 2px rgba(0,0,0,0.6)"
            :touch-action "none"}}
   [:div.flex.mb-1 {:style {:align-items "center"}}
    [:span.inline-block.border-safe-dark.border-solid.border-2.rounded-full.mr-2
     {:class "bg-safe/75"
      :style {:width 25 :height 25}}
     ""]
    "Safe spot"]
   [:div.flex.mb-1 {:style {:align-items "center"}}
    [:span.bg-unsafe.inline-block.border-unsafe-dark.border-solid.border-2.rounded-full.mr-2
     {:class "bg-unsafe/75"
      :style {:width 25 :height 25}}
     ""]
    "Unsafe spot"]
   [:div.flex.mb-1
    [:img.mr-2 {:src "img/separated.svg"
                :width 25 :height 25}]
    "Separated lane/trail"]
   [:div.flex.mb-1
    [:img.mr-2 {:src "img/on_road.svg"
                :width 25 :height 25}]
    "On-road bike lane"]
   [:div.text-sm.mt-2 "Spot size represents" [:br] "submission activity"]])

(defn header [about-handler data-handler]
  [:div.flex.px-3.md:px-4.py-1.md:py-3
   {:style {:justify-content "space-between"
            :align-items "center"}}
   [:div.flex.gap-2.md:gap-4
    [:div.flex.gap-3
     [:a {:href "https://www.bikespot.org"
          :target "_blank"
          :style {:flex-shrink "0"}}
      [:img.block {:src "/img/bikespot-logo.svg"
                   :style {:max-height 50}}]]
     [:span.font-bold.tracking-tight.mr-1.md:mr-4
     {:class [(str "text-l md:text-[22px]/7")]
      :style {:align-self "flex-end"}}
     "Archive Map"]]
    [:button.md:text-xl.text-white.transition-all.font-bold.px-3.py-2.rounded-sm
     {:class "bg-[#2886bb] hover:bg-[#216e9a]"
      :on-click about-handler}
     "About"]
   [:button.md:text-xl.text-white.transition-all.font-bold.px-3.py-2.rounded-sm
     {:class "bg-[#2886bb] hover:bg-[#216e9a]"
      :on-click data-handler
      :title "Download source data"}
     "Data"]]
   [:div
    [:a {:href "https://www.crowdspot.com.au/"
         :target "_blank"}
     [:img.hidden.md:block.mt-2 {:src "/img/crowdspot-logo.svg"
                                 :style {:max-height 30
                                         :alt "CrowdSpot"}}]]]])

(defn footer []
  [:div.md:hidden.px-2.py-1
   [:a {:href "https://www.crowdspot.com.au/"
        :target "_blank"}
    [:img.block.mx-auto {:src "/img/crowdspot-logo.svg"
                         :style {:max-height 25
                                 :alt "CrowdSpot"}}]]])
